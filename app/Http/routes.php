<?php

Route::get('/', function(){ return redirect('erp'); });

// Creating a resource for ERP for RESTful purposes
Route::resource('erp', 'ErpController');