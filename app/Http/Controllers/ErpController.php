<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Erp;

use App\Helpers\ImageProcessor;

class ErpController extends Controller
{
    protected $erp;
    protected $imageProcessor;

    public function __construct()
    {
        $this->erp = new Erp();
        $this->imageProcessor = new ImageProcessor();
    }

    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index()
    {
        // Paginate records by 5 records per page
        $records = $this->erp->paginate(5);
        return view('erp.index', ['records' => $records]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create()
    {
        return view('erp.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  Request  $request
     * @return Response
     */
    public function store(Request $request)
    {
        $data = $request->all();

        $photos = ['photo_ID', 'photo_ID2', 'photo_lic', 'photo_lic2', 
                    'photo_pas', 'picture', 'fingerprint1', 'fingerprint2'];

        foreach($photos as $photo){
            if(isset($data[$photo])){
                $data[$photo] = $this->imageProcessor->process($photo, $data[$photo]);
            }
        }

        $this->erp->create($data);

        return redirect('/erp');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function show($id)
    {
        $record = $this->erp->findOrFail($id);

        return view('erp.show')->with('record', $record);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  Request  $request
     * @param  int  $id
     * @return Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function destroy($id)
    {
        //
    }
}
