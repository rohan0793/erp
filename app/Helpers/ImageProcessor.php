<?php

namespace App\Helpers;

use Intervention\Image\ImageManager;

class ImageProcessor{

	protected $manager;

    public function __construct()
    {
        $this->manager = new ImageManager();
    }

	public function process($id, $image){
		// Taking a random string so that the image path does not collide
        // with another image in the file system
        $fileName = str_random(40);

        // Using Image Intervention to resize and store the image easily.
        // This library can also be used to crop and do other useful stuff with images
        $image = $this->manager->make($image)->resize(100, 100);

        // Save it to a folder with the random name
        $image->save(public_path() . '/photos/' . $fileName . '.jpg');

        // Return the path which will be stored in the DB and
        // later used to construct anchor and img tags
        return '/photos/' . $fileName . '.jpg';
	}
}