<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Erp extends Model
{
    // Because the table name is not as per conventions
    protected $table = 'erp';

    // Protect against mass assignment
    protected $fillable = [
    	'email1',
    	'email2',
    	'email3',
    	'home_phone',
    	'fax',
    	'cellphone1',
    	'cellphone2',
    	'cellphone3',
    	'facebook',
    	'twitter',
    	'google',
    	'website',
    	'assistant',
    	'organization',
    	'position',
    	'photo_ID',
    	'photo_ID2',
    	'photo_lic',
    	'photo_lic2',
    	'photo_pas',
    	'picture',
    	'fingerprint1',
    	'fingerprint2',
    ];
}
