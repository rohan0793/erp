<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateErpTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('erp', function (Blueprint $table) {
            $table->increments('id');
            $table->string('email1');
            $table->string('email2')->nullable();
            $table->string('email3')->nullable();
            $table->string('home_phone')->nullable();
            $table->string('fax')->nullable();
            $table->string('cellphone1');
            $table->string('cellphone2')->nullable();
            $table->string('cellphone3')->nullable();

            $table->string('facebook')->nullable();
            $table->string('twitter')->nullable();
            $table->string('google')->nullable();
            $table->string('website')->nullable();
            $table->string('assistant')->nullable();
            $table->string('organization')->nullable();
            $table->string('position')->nullable();

            $table->string('photo_ID')->nullable();
            $table->string('photo_ID2')->nullable();
            $table->string('photo_lic')->nullable();
            $table->string('photo_lic2')->nullable();
            $table->string('photo_pas')->nullable();
            $table->string('picture')->nullable();
            $table->string('fingerprint1')->nullable();
            $table->string('fingerprint2')->nullable();

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('erp');
    }
}
