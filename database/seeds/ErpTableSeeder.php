<?php

use Illuminate\Database\Seeder;

class ErpTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker = Faker\Factory::create();

        for($i = 0; $i < 100; $i++){
        	App\Erp::create([
	        	'email1' => $faker->email(),
	        	'email2' => $faker->email(),
	        	'email3' => $faker->email(),

	        	'home_phone' => $faker->phoneNumber(),
	        	'fax' => $faker->phoneNumber(),

	        	'cellphone1' => $faker->phoneNumber(),
	        	'cellphone1' => $faker->phoneNumber(),
	        	'cellphone1' => $faker->phoneNumber(),

	        	'facebook' => $faker->word(),
	        	'twitter' => $faker->word(),
	        	'google' => $faker->word(),
	        	'website' => $faker->word() . 'com',
	        	'assistant' => $faker->firstName() . $faker->lastName(),
	        	'organization' => $faker->catchPhrase(),
	        	'position' => $faker->word(),

	        	'photo_ID' => $faker->imageUrl($width = 100, $height = 100),
	        	'photo_ID2' => $faker->imageUrl($width = 100, $height = 100),
	        	'photo_lic' => $faker->imageUrl($width = 100, $height = 100),
	        	'photo_lic2' => $faker->imageUrl($width = 100, $height = 100),
	        	'photo_pas' => $faker->imageUrl($width = 100, $height = 100),
	        	'picture' => $faker->imageUrl($width = 100, $height = 100),
	        	'fingerprint1' => $faker->imageUrl($width = 100, $height = 100),
	        	'fingerprint2' => $faker->imageUrl($width = 100, $height = 100),
	        ]);
        } 
    }
}
