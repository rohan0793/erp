<html>
<head>
	<title>ERP</title>

	<link rel="stylesheet" type="text/css" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap.min.css">
	<link rel="stylesheet" type="text/css" href="//cdn.datatables.net/1.10.8/css/jquery.dataTables.min.css">
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.4.0/css/font-awesome.min.css">
</head>
<body>
	<div style="height:25px"></div>
	<div class="container">
		<div class="row">
			<div class="col-xs-12">
				<a href="/erp/create" class="btn btn-success btn-lg pull-right"><i class="fa fa-edit"></i> Create New</a>
				<h1 class="text-center">ERP Table</h1>
				<div class="table-responsive">
					<table class="table">

						<tr>
			                <th>Email</th>
			                <th>Phone</th>
			                <th>Fax</th>
			                <th>Facebook</th>
			                <th>Twitter</th>
			                <th>PhotoID</th>
			                <th>View Details</th>
			            </tr>

			            <tbody>

				            @foreach($records as $record)

				            	<tr>
					                <td>{{ $record->email1 }}</td>
					                <td>{{ $record->home_phone }}</td>
					                <td>{{ $record->fax }}</td>
					                <td>{{ $record->facebook }}</td>
					                <td>{{ $record->twitter }}</td>
					                <td><a href="{{ $record->photo_ID }}"><img src="{{ $record->photo_ID }}"></a></td>
					                <td class="text-center"><a href="/erp/{{ $record->id }}" class="btn btn-default">Details</a href="/erp/{{}}"></td>
					            </tr>

				            @endforeach

			            </tbody>

					</table>
				</div>
				{!! $records->render() !!}

			</div>
		</div>
	</div>

	<script src="//ajax.googleapis.com/ajax/libs/jquery/2.1.1/jquery.min.js"></script>
	<script src="//ajax.googleapis.com/ajax/libs/jqueryui/1.10.3/jquery-ui.min.js"></script>
	<script type="text/javascript" src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/js/bootstrap.min.js"></script>
</body>
</html>