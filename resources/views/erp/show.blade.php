<html>
<head>
	<title>ERP</title>

	<link rel="stylesheet" type="text/css" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap.min.css">
	<link rel="stylesheet" type="text/css" href="//cdn.datatables.net/1.10.8/css/jquery.dataTables.min.css">

	<style type="text/css">
		img{
			width: 100px;
			height: 100px;
		}
	</style>
</head>
<body>
	<div class="container">
		<div class="row">
			<div class="col-xs-12">
				<h1 class="text-center">Details</h1>
				<div class="table-responsive">
					<table class="table">
						<tbody>
							<tr>
								<td><b>Email</b></td>
								<td class="text-right">{{ $record->email1 }}</td>
							</tr>
							<tr>
								<td><b>Email 2</b></td>
								<td class="text-right">{{ $record->email2 }}</td>
							</tr>
							<tr>
								<td><b>Email 3</b></td>
								<td class="text-right">{{ $record->email3 }}</td>
							</tr>

							<tr>
								<td><b>Home Phone</b></td>
								<td class="text-right">{{ $record->home_phone }}</td>
							</tr>
							<tr>
								<td><b>Fax</b></td>
								<td class="text-right">{{ $record->fax }}</td>
							</tr>
							<tr>
								<td><b>Cellphone 1</b></td>
								<td class="text-right">{{ $record->cellphone1 }}</td>
							</tr>
							<tr>
								<td><b>Cellphone 2</b></td>
								<td class="text-right">{{ $record->cellphone2 }}</td>
							</tr>
							<tr>
								<td><b>Cellphone 3</b></td>
								<td class="text-right">{{ $record->cellphone3 }}</td>
							</tr>

							<tr>
								<td><b>Facebook</b></td>
								<td class="text-right">{{ $record->facebook }}</td>
							</tr>
							<tr>
								<td><b>Twitter</b></td>
								<td class="text-right">{{ $record->twitter }}</td>
							</tr>
							<tr>
								<td><b>Google</b></td>
								<td class="text-right">{{ $record->google }}</td>
							</tr>
							<tr>
								<td><b>Website</b></td>
								<td class="text-right">{{ $record->website }}</td>
							</tr>
							<tr>
								<td><b>Assistant</b></td>
								<td class="text-right">{{ $record->assistant }}</td>
							</tr>
							<tr>
								<td><b>Organization</b></td>
								<td class="text-right">{{ $record->organization }}</td>
							</tr>
							<tr>
								<td><b>Position</b></td>
								<td class="text-right">{{ $record->position }}</td>
							</tr>

							<tr>
								<td><b>Photo ID</b></td>
								<td class="text-right">
									@if( $record->photo_ID != '' )
										<a target="_blank" href="{{ $record->photo_ID }}"><img src="{{ $record->photo_ID }}"></a>
									@endif
								</td>
							</tr>
							<tr>
								<td><b>Photo ID 2</b></td>
								<td class="text-right">
									@if( $record->photo_ID2 != '' )
										<a target="_blank" href="{{ $record->photo_ID2 }}"><img src="{{ $record->photo_ID2 }}"></a>
									@endif
								</td>
							</tr>
							<tr>
								<td><b>License</b></td>
								<td class="text-right">
									@if( $record->photo_lic != '' )
										<a target="_blank" href="{{ $record->photo_lic }}"><img src="{{ $record->photo_lic }}"></a>
									@endif
								</td>
							</tr>
							<tr>
								<td><b>License 2</b></td>
								<td class="text-right">
									@if( $record->photo_lic2 != '' )
										<a target="_blank" href="{{ $record->photo_lic2 }}"><img src="{{ $record->photo_lic2 }}"></a>
									@endif
								</td>
							</tr>
							<tr>
								<td><b>Passport Photo</b></td>
								<td class="text-right">
									@if( $record->photo_pas != '' )
										<a target="_blank" href="{{ $record->photo_pas }}"><img src="{{ $record->photo_pas }}"></a>
									@endif
								</td>
							</tr>
							<tr>
								<td><b>Picture</b></td>
								<td class="text-right">
									@if( $record->picture != '' )
										<a target="_blank" href="{{ $record->picture }}"><img src="{{ $record->picture }}"></a>
									@endif
								</td>
							</tr>
							<tr>
								<td><b>Fingerprint 1</b></td>
								<td class="text-right">
									@if( $record->fingerprint1 != '' )
										<a target="_blank" href="{{ $record->fingerprint1 }}"><img src="{{ $record->fingerprint1 }}"></a>
									@endif
								</td>
							</tr>
							<tr>
								<td><b>Fingerprint 2</b></td>
								<td class="text-right">
									@if( $record->fingerprint2 != '' )
										<a target="_blank" href="{{ $record->fingerprint2 }}"><img src="{{ $record->fingerprint2 }}"></a>
									@endif
								</td>
							</tr>
						</tbody>
					</table>
				</div>
			</div>
		</div>
	</div>

	<script src="//ajax.googleapis.com/ajax/libs/jquery/2.1.1/jquery.min.js"></script>
	<script src="//ajax.googleapis.com/ajax/libs/jqueryui/1.10.3/jquery-ui.min.js"></script>
	<script type="text/javascript" src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/js/bootstrap.min.js"></script>
</body>
</html>