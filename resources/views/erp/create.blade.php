<html>
<head>
	<title>ERP Create</title>

	<link rel="stylesheet" type="text/css" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap.min.css">
	<link rel="stylesheet" type="text/css" href="//cdn.datatables.net/1.10.8/css/jquery.dataTables.min.css">
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.4.0/css/font-awesome.min.css">
</head>
<body>
	<div class="container">
		<h1 class="text-center">Create New Resource</h1>
		<form id="erpForm" method="POST" enctype="multipart/form-data" action="/erp">
			<input type="hidden" name="_token" value="{{ csrf_token() }}">
			<div class="row">
				<div class="col-md-4">
					<div class="form-group">
						<div class="input-group">
							<div class="input-group-addon"><i class="fa fa-envelope"></i></div>
							<input name="email1" type="email" class="form-control" placeholder="Email 1 (Required)" required />
						</div>
					</div>
				</div>

				<div class="col-md-4">
					<div class="form-group">
						<div class="input-group">
							<div class="input-group-addon"><i class="fa fa-envelope"></i></div>
							<input name="email2" type="email" class="form-control" placeholder="Email 2">
						</div>
					</div>
				</div>

				<div class="col-md-4">
					<div class="form-group">
						<div class="input-group">
							<div class="input-group-addon"><i class="fa fa-envelope"></i></div>
							<input name="email3" type="email" class="form-control" placeholder="Email 3">
						</div>
					</div>
				</div>
			</div>

			<div class="row">
				<div class="col-md-4">
					<div class="form-group">
						<div class="input-group">
							<div class="input-group-addon"><i class="fa fa-phone"></i></div>
							<input name="home_phone" type="text" class="form-control" placeholder="Home Phone">
						</div>
					</div>
				</div>

				<div class="col-md-4">
					<div class="form-group">
						<div class="input-group">
							<div class="input-group-addon"><i class="fa fa-phone"></i></div>
							<input name="fax" type="text" class="form-control" placeholder="Fax">
						</div>
					</div>
				</div>

				<div class="col-md-4">
					<div class="form-group">
						<div class="input-group">
							<div class="input-group-addon"><i class="fa fa-user"></i></div>
							<input name="position" type="text" class="form-control" placeholder="Position">
						</div>
					</div>
				</div>
			</div>

			<div class="row">
				<div class="col-md-4">
					<div class="form-group">
						<div class="input-group">
							<div class="input-group-addon"><i class="fa fa-phone"></i></div>
							<input name="cellphone1" type="text" class="form-control" placeholder="Cellphone 1 (Required)" required>
						</div>
					</div>
				</div>

				<div class="col-md-4">
					<div class="form-group">
						<div class="input-group">
							<div class="input-group-addon"><i class="fa fa-phone"></i></div>
							<input name="cellphone2" type="text" class="form-control" placeholder="Cellphone 2">
						</div>
					</div>
				</div>

				<div class="col-md-4">
					<div class="form-group">
						<div class="input-group">
							<div class="input-group-addon"><i class="fa fa-phone"></i></div>
							<input name="cellphone3" type="text" class="form-control" placeholder="Cellphone 3">
						</div>
					</div>
				</div>
			</div>

			<div class="row">
				<div class="col-md-4">
					<div class="form-group">
						<div class="input-group">
							<div class="input-group-addon"><i class="fa fa-facebook"></i></div>
							<input name="facebook" type="text" class="form-control" placeholder="Facebook Handle">
						</div>
					</div>
				</div>

				<div class="col-md-4">
					<div class="form-group">
						<div class="input-group">
							<div class="input-group-addon"><i class="fa fa-twitter"></i></div>
							<input name="twitter" type="text" class="form-control" placeholder="Twitter Handle">
						</div>
					</div>
				</div>

				<div class="col-md-4">
					<div class="form-group">
						<div class="input-group">
							<div class="input-group-addon"><i class="fa fa-google"></i></div>
							<input name="google" type="text" class="form-control" placeholder="Google Handle">
						</div>
					</div>
				</div>
			</div>

			<div class="row">
				<div class="col-md-4">
					<div class="form-group">
						<div class="input-group">
							<div class="input-group-addon"><i class="fa fa-link"></i></div>
							<input name="website" type="url" class="form-control" placeholder="Website">
						</div>
					</div>
				</div>

				<div class="col-md-4">
					<div class="form-group">
						<div class="input-group">
							<div class="input-group-addon"><i class="fa fa-user"></i></div>
							<input name="assistant" type="text" class="form-control" placeholder="Assistant">
						</div>
					</div>
				</div>

				<div class="col-md-4">
					<div class="form-group">
						<div class="input-group">
							<div class="input-group-addon"><i class="fa fa-building"></i></div>
							<input name="organization" type="text" class="form-control" placeholder="Organization">
						</div>
					</div>
				</div>
			</div>

			<div class="row">
				<div class="col-md-3">
					<div class="form-group">
						<label for="exampleInputFile">Photo ID 1</label>
						<input name="photo_ID" type="file">
						<p class="help-block">Front of Photo ID.</p>
					</div>
				</div>

				<div class="col-md-3">
					<div class="form-group">
						<label for="exampleInputFile">Photo ID 2</label>
						<input name="photo_ID2" type="file">
						<p class="help-block">Back of Photo ID.</p>
					</div>
				</div>

				<div class="col-md-3">
					<div class="form-group">
						<label for="exampleInputFile">License 1</label>
						<input name="photo_lic" type="file">
						<p class="help-block">Front of License.</p>
					</div>
				</div>

				<div class="col-md-3">
					<div class="form-group">
						<label for="exampleInputFile">License 2</label>
						<input name="photo_lic2" type="file">
						<p class="help-block">Back of License.</p>
					</div>
				</div>
			</div>

			<div class="row">
				<div class="col-md-3">
					<div class="form-group">
						<label for="exampleInputFile">Photo Passport</label>
						<input name="photo_pas" type="file">
						<p class="help-block">Photo of passport.</p>
					</div>
				</div>

				<div class="col-md-3">
					<div class="form-group">
						<label for="exampleInputFile">Picture</label>
						<input name="picture" type="file">
						<p class="help-block">Picture.</p>
					</div>
				</div>

				<div class="col-md-3">
					<div class="form-group">
						<label for="exampleInputFile">Fingerprint 1</label>
						<input name="fingerprint1" type="file">
						<p class="help-block">Fingerprint 1.</p>
					</div>
				</div>

				<div class="col-md-3">
					<div class="form-group">
						<label for="exampleInputFile">Fingerprint 2</label>
						<input name="fingerprint2" type="file">
						<p class="help-block">Fingerprint 2.</p>
					</div>
				</div>
			</div>

			<div class="row">
				<div class="col-md-3">
				</div>

				<div class="col-md-3">
				</div>

				<div class="col-md-3">
					
				</div>

				<div class="col-md-3">
					<button type="reset" class="btn btn-default">Reset</button>
					<button type="submit" class="btn btn-primary">Submit</button>
				</div>
			</div>
		</form>
	</div>

	<script src="//ajax.googleapis.com/ajax/libs/jquery/2.1.1/jquery.min.js"></script>
	<script src="//ajax.googleapis.com/ajax/libs/jqueryui/1.10.3/jquery-ui.min.js"></script>
	<script type="text/javascript" src="http://ajax.aspnetcdn.com/ajax/jquery.validate/1.14.0/jquery.validate.min.js"></script>
	<script type="text/javascript" src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/js/bootstrap.min.js"></script>

	<script type="text/javascript">

		$.validator.setDefaults({
			highlight: function(element) {
				$(element).closest('.form-group').addClass('has-error');
			},
			unhighlight: function(element) {
				$(element).closest('.form-group').removeClass('has-error');
			},
			errorElement: 'span',
			errorClass: 'help-block',
			errorPlacement: function(error, element) {
				if(element.parent('.input-group').length) {
					error.insertAfter(element.parent());
				} else {
					error.insertAfter(element);
			}
			}
		});
		$('#erpForm').validate();

	</script>
</body>
</html>